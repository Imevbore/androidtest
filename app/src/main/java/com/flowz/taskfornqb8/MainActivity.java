package com.flowz.taskfornqb8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flowz.taskfornqb8.notification.NotificationEventReceiver;

public class MainActivity extends AppCompatActivity {
    Button startNot, login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startNot = findViewById(R.id.start_notification);
        login = findViewById(R.id.login);
    }

    public void onSendNotificationsButtonClick(View view) {
        NotificationEventReceiver.setupAlarm(getApplicationContext());
        Toast.makeText(this, "Notification Service Started", Toast.LENGTH_SHORT).show();
    }
}
