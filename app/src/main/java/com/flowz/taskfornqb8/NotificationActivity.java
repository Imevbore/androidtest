package com.flowz.taskfornqb8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class NotificationActivity extends AppCompatActivity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        image = findViewById(R.id.image1);

        //Glide.with(MainActivity.this).load(R.drawable.johnywalker).into(new GlideDrawableImageViewTarget(image));
        Glide.with(this).load(R.raw.johnywalker).into(image);
    }
}
